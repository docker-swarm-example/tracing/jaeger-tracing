# Jaeger

## Prerequisites

### Traefik
This project makes use of [Traefik](https://traefik.io) to route ingress traffic to the Jaeger dashboard. You can deploy the following [project](https://gitlab.com/docker-swarm-example/ingress/traefik-ingress) to get Traefik up and running in your Docker swarm.

### Elasticsearch
All spans and traces are stored in Elasticsearch, so you will need to deploy Elasticsearch to your Docker swarm. You can deploy the following [project](https://gitlab.com/docker-swarm-example/logging/efk-logging) to get Elasticsearch running in your Docker swarm.

## Deploy

Execute the following commands to deploy Jaeger to Docker swarm:

```sh
git clone https://gitlab.com/docker-swarm-example/tracing/jaeger-tracing.git
docker stack deploy --compose-file jaeger-tracing/docker-compose.yml tracing
```

After deployment you can access the Jaeger dashboard by navigating to http://your.domain/jaeger.
